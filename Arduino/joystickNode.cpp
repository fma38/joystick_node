/*
    joystickNode.cpp

    (C) 2020 Frédéric Mantegazza

    Licenced under GPL:

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:

    - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    - Neither the name of the nor the names of its contributors may be used to endorse or
    promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
    FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "joystickNode.h"


JoystickNode::JoystickNode(TwoWire& wire, uint8_t i2cAddress) :
    m_wire(&wire),
    m_i2cAddress(i2cAddress)
{
    this->refresh();
}

void JoystickNode::refresh()
{
    m_inputs = i2cRead(REG_INPUTS);

    m_adc[0] = i2cRead(REG_ADC_0);
    m_adc[1] = i2cRead(REG_ADC_1);
    m_adc[2] = i2cRead(REG_ADC_2);
    m_adc[3] = i2cRead(REG_ADC_3);
}

uint8_t JoystickNode::getInput(uint8_t inputNum)
{
    return (m_inputs & (1 << inputNum)) >> inputNum;
}

uint8_t JoystickNode::getAdc(uint8_t adcNum)
{
    return m_adc[adcNum];
}

uint8_t JoystickNode::readConfig()
{
    return this->i2cRead(REG_CONFIG);
}

void JoystickNode::writeConfig(uint8_t config)
{
    this->i2cWrite(REG_CONFIG, config);
}

uint8_t JoystickNode::i2cRead(uint8_t reg)
{
    m_wire->beginTransmission(m_i2cAddress);
    m_wire->write(reg);
    m_wire->endTransmission(true);
    m_wire->requestFrom(m_i2cAddress, (uint8_t)1);

    if (m_wire->available() == 1) {
        return m_wire->read();
    }

    else {
        return 0xaa;
    }
}

void JoystickNode::i2cWrite(uint8_t reg, uint8_t value)
{
    m_wire->beginTransmission(m_i2cAddress);
    m_wire->write(reg);
    m_wire->write(value);
    m_wire->endTransmission(true);
}
