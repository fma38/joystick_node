#include <Wire.h>

#include "joystickNode.h"

JoystickNode* g_joystick;


void setup()
{
    // Serial port (debug)
    Serial.begin(115200);

    // I²C
    Wire.begin();

    // Joystick
    g_joystick = new JoystickNode(Wire, 0x10);
//     g_joystick->writeConfig(JoystickNode::INVERT_ADC_0);
}


void loop()
{
    g_joystick->refresh();

    Serial.print(g_joystick->getAdc(JoystickNode::ADC_0)); Serial.print(", ");
    Serial.print(g_joystick->getAdc(JoystickNode::ADC_1)); Serial.print(", ");
    Serial.print(g_joystick->getAdc(JoystickNode::ADC_2)); Serial.print(", ");
    Serial.print(g_joystick->getAdc(JoystickNode::ADC_3)); Serial.print(", ");

    Serial.print(g_joystick->getInput(JoystickNode::INPUT_0)); Serial.print(", ");
    Serial.print(g_joystick->getInput(JoystickNode::INPUT_1)); Serial.print(", ");
    Serial.print(g_joystick->getInput(JoystickNode::INPUT_2)); Serial.print(", ");
    Serial.print(g_joystick->getInput(JoystickNode::INPUT_3)); Serial.println();
}
