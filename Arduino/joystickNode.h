/*
    joystickNode.h

    (C) 2020 Frédéric Mantegazza

    Licenced under GPL:

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:

    - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    - Neither the name of the nor the names of its contributors may be used to endorse or
    promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
    FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef JOYSTICK_NODE_H
#define JOYSTICK_NODE_H

#include <Wire.h>

class JoystickNode
{
public:
    enum i2cRegisters {
        REG_INPUTS = 0x00,  // RO,  uint8_t
        REG_ADC_0  = 0x01,  // RO,  uint8_t
        REG_ADC_1  = 0x02,  // RO,  uint8_t
        REG_ADC_2  = 0x03,  // RO,  uint8_t
        REG_ADC_3  = 0x04,  // RO,  uint8_t
        REG_CONFIG = 0x05   // R/W, uint8_t
    };

    enum config {
        INVERT_INPUT_0 = 0x01,  // 0=normal, 1=inverted
        INVERT_INPUT_1 = 0x02,  // 0=normal, 1=inverted
        INVERT_INPUT_2 = 0x03,  // 0=normal, 1=inverted
        INVERT_INPUT_3 = 0x08,  // 0=normal, 1=inverted
        INVERT_ADC_0 = 0x10,    // 0=normal, 1=inverted
        INVERT_ADC_1 = 0x20,    // 0=normal, 1=inverted
        INVERT_ADC_2 = 0x40,    // 0=normal, 1=inverted
        INVERT_ADC_3 = 0x80     // 0=normal, 1=inverted
    };

    enum analog {
        ADC_0 = 0,
        ADC_1 = 1,
        ADC_2 = 2,
        ADC_3 = 3
    };

    enum inputs {
        INPUT_0 = 0,
        INPUT_1 = 1,
        INPUT_2 = 2,
        INPUT_3 = 3
    };

public:
    JoystickNode(TwoWire& wire, uint8_t i2cAddress=0x10);

    void refresh();

    uint8_t getInput(uint8_t inputNum);
    uint8_t getAdc(uint8_t adcNum);

    uint8_t readConfig();
    void writeConfig(uint8_t config);

private:
    TwoWire* m_wire;
    uint8_t m_i2cAddress;

    uint8_t m_inputs;
    uint8_t m_adc[4];

    uint8_t i2cRead(uint8_t reg);
    void i2cWrite(uint8_t reg, uint8_t value);
};

#endif
