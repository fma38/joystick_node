/*
    joystick_node.c

    (C) 2020 Frédéric Mantegazza

    I²C handling based on Hisashi Ito code (https://github.com/orangkucing/WireS)

    Licenced under GPL:

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:

    - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    - Neither the name of the nor the names of its contributors may be used to endorse or
    promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
    FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Includes
#include <stdint.h>
#include <stddef.h>
#include <avr/io.h>
#include <avr/interrupt.h>

// Hardware config
#define I2C_SLAVE_ADDRESS 0x10
#define I2C_SLAVE_MASK    0x00
#define I2C_BUFFER_LENGTH    8

#define INPUT_BIT_0  0
#define INPUT_BIT_1  1
#define INPUT_BIT_2  2
#define INPUT_BIT_3  3

#define GET_INPUT_0 (((PINA & _BV(PA7)) >> PA7) << INPUT_BIT_0)
#define GET_INPUT_1 (((PINB & _BV(PB0)) >> PB0) << INPUT_BIT_1)
#define GET_INPUT_2 (((PINB & _BV(PB1)) >> PB1) << INPUT_BIT_2)
#define GET_INPUT_3 (((PINB & _BV(PB2)) >> PB2) << INPUT_BIT_3)

// I2C registers
#define REG_INPUTS 0x00  // RO,  uint8_t
#define REG_ADC_0  0x01  // RO,  uint8_t
#define REG_ADC_1  0x02  // RO,  uint8_t
#define REG_ADC_2  0x03  // RO,  uint8_t
#define REG_ADC_3  0x04  // RO,  uint8_t
#define REG_CONFIG 0x05  // R/W, uint8_t

// Config
#define CONFIG_INVERT_INPUT_0 _BV(0)  // 0=normal, 1=inverted
#define CONFIG_INVERT_INPUT_1 _BV(1)  // 0=normal, 1=inverted
#define CONFIG_INVERT_INPUT_2 _BV(2)  // 0=normal, 1=inverted
#define CONFIG_INVERT_INPUT_3 _BV(3)  // 0=normal, 1=inverted
#define CONFIG_INVERT_ADC_0 _BV(4)    // 0=normal, 1=inverted
#define CONFIG_INVERT_ADC_1 _BV(5)    // 0=normal, 1=inverted
#define CONFIG_INVERT_ADC_2 _BV(6)    // 0=normal, 1=inverted
#define CONFIG_INVERT_ADC_3 _BV(7)    // 0=normal, 1=inverted

// Types
struct i2cStruct
{
    uint8_t buffer[I2C_BUFFER_LENGTH];  // Tx/Rx Buffer         (ISR)
    size_t rxBufferIndex;               // Rx Index             (User & ISR)
    size_t rxBufferLength;              // Rx Length            (ISR)
    size_t txBufferIndex;               // Tx Index             (User & ISR)
    size_t txBufferLength;              // Tx Length            (User & ISR)
    int8_t startCount;                  // repeated START count (User & ISR)
    uint16_t addr;                      // Tx/Rx address        (User & ISR)
};

// Global vars
static volatile struct i2cStruct g_i2c;  // i2c data
static volatile uint8_t g_register;      // register
static volatile uint8_t g_inputs;        // inputs state
static volatile uint8_t g_adc[4];        // adc values
static volatile uint8_t g_config;        // inputs configuration (implement level inversion)


// Init helpers
static void initIO(void)
{
    // Enable pullup resistors on inputs
    PUEA = _BV(PA7);  // IO0
    PUEB = _BV(PB0)   // IO1
         | _BV(PB1)   // IO2
         | _BV(PB2);  // IO3

    // Pull-up unused pins
    PUEA |= _BV(PA5);  // MISO
}


static void initAdc(void)
{
    ADCSRA = _BV(ADEN)    // enable ADC
           | _BV(ADPS1)   // clock divider by 8
           | _BV(ADPS0);  // clock divider by 8

    ADCSRB = _BV(ADLAR);  // result is left adjusted

    // Select ADC input
    ADMUXA = 0x00;  // ADC0

    // Select voltage ref.
    ADMUXB = 0x00;  // Vcc, gain 1

    // Disable digital input on ADC0, ADC1, ADC2, ADC3
    DIDR0 = _BV(ADC0D)
          | _BV(ADC1D)
          | _BV(ADC2D)
          | _BV(ADC3D);
}


static void initI2C(void)
{
    // Set slave address and mask
    TWSA = (I2C_SLAVE_ADDRESS << 1)  // slave address
         | _BV(0);                   // allow general call address recognition (broadcast)
    TWSAM = I2C_SLAVE_MASK;

    // Init slave data register
    TWSD = 0xFF;

    g_i2c.startCount = -1;

    TWSCRA = _BV(TWSHE)   // TWI SDA Hold Time Enable
           | _BV(TWDIE)   // TWI Data Interrupt Enable
           | _BV(TWASIE)  // TWI Address/Stop Interrupt Enable
           | _BV(TWEN)    // TWI Interface Enable
           | _BV(TWSIE);  // TWI Stop Interrupt Enable
}


// I2C helpers
static int i2cAvailable(void)
{
    return g_i2c.rxBufferLength - g_i2c.rxBufferIndex;
}


static size_t i2cWrite8(uint8_t data)
{
    if (g_i2c.txBufferLength < I2C_BUFFER_LENGTH) {
        g_i2c.buffer[g_i2c.txBufferLength++] = data;
        return 1;
    }
    return 0;
}


static uint8_t i2cRead8(void)
{
    if (g_i2c.rxBufferIndex >= g_i2c.rxBufferLength) {
        return 0;
    }
    return g_i2c.buffer[g_i2c.rxBufferIndex++];
}


// static size_t i2cWrite16(uint16_t data)
// {
//     if ((g_i2c.txBufferLength+1) < I2C_BUFFER_LENGTH) {
//         g_i2c.buffer[g_i2c.txBufferLength++] = (uint8_t)(data & 0xff);
//         g_i2c.buffer[g_i2c.txBufferLength++] = (uint8_t)((data >> 8) & 0xff);
//        return 2;
//     }
//     return 0;
// }
//
//
// static uint16_t i2cRead16(void)
// {
//     if ((g_i2c.rxBufferIndex+1) >= g_i2c.rxBufferLength) {
//         return 0;
//     }
//     uint8_t low = g_i2c.buffer[g_i2c.rxBufferIndex++];
//     uint8_t high = g_i2c.buffer[g_i2c.rxBufferIndex++];
//     return ((uint16_t)high << 8) | low;
// }
//
//
// static size_t i2cWriteBlock(const uint8_t* data, size_t quantity)
// {
//     if (g_i2c.txBufferLength < I2C_BUFFER_LENGTH) {
//         size_t avail = I2C_BUFFER_LENGTH - g_i2c.txBufferLength;
//         volatile uint8_t* dest = g_i2c.buffer + g_i2c.txBufferLength;
//
//         // Truncate to space avail if needed
//         if (quantity > avail) {
//             quantity = avail;
//         }
//
//         for (size_t count=quantity; count; count--) {
//             *dest++ = *data++;
//         }
//
//         g_i2c.txBufferLength += quantity;
//
//         return quantity;
//     }
//     return 0;
// }
//
//
// static uint16_t i2cReadBlock(uint8_t* data, size_t quantity)
// {
//     if ((g_i2c.rxBufferIndex+quantity) >= g_i2c.rxBufferLength) {
//         return 0;
//     }
//     for (size_t count=quantity; count; count--) {
//         *data++ = g_i2c.buffer[g_i2c.rxBufferIndex++];
//     }
//     return 1;
// }


// I2C callbacks
static uint8_t onI2cAddrReceive(uint16_t addr, uint8_t startCount)
{
    return 1;
}


static void onI2cReceive(size_t nb)
{
    if (nb >= 1) {
        g_register = i2cRead8();
        nb--;

        switch (g_register) {

            case REG_INPUTS:

                // This register is RO
                break;

            case REG_ADC_0:
            case REG_ADC_1:
            case REG_ADC_2:
            case REG_ADC_3:

                // These registers are RO
                break;

            case REG_CONFIG:
                if (nb == 1) {

                    // Read configuration
                    g_config = i2cRead8();
                }
                break;

            default:
                break;
        }

        // Purge buffer
        while (i2cAvailable()) {
            i2cRead8();
        }
    }
}


static void onI2cRequest(void)
{
    switch (g_register) {

        case REG_INPUTS:
            i2cWrite8(g_inputs);
            break;

        case REG_ADC_0:
        case REG_ADC_1:
        case REG_ADC_2:
        case REG_ADC_3:
            i2cWrite8(g_adc[g_register-REG_ADC_0]);
            break;

        case REG_CONFIG:
            i2cWrite8(g_config);
            break;

        default:
            break;
    }
}


static void onI2cStop(void)
{
}


// I2C interrupt routine
ISR(TWI_SLAVE_vect)
{
    uint8_t i2cStatus = TWSSRA;

    // Bus error or transmit collision
    if ((i2cStatus & (_BV(TWC) | _BV(TWBE)))) {
        g_i2c.startCount = -1;
        TWSSRA |= _BV(TWASIF)  // clear TWI Address/Stop Interrupt Flag
               |  _BV(TWDIF)   // clear TWI Data Interrupt Flag
               |  _BV(TWBE);   // clear TWI Bus Error
        return;
    }

    // Valid address has been received or Stop condition detected
    if (i2cStatus & _BV(TWASIF)) {

        // Valid address has been received (after Start condition)
        if (i2cStatus & _BV(TWAS)) {
            g_i2c.addr = TWSD;
            g_i2c.startCount++;
            if ((g_i2c.addr & 0b11111001) == 0b11110000) {

                // Send ACK
                TWSCRB = _BV(TWHNM)
                       | _BV(TWCMD1)
                       | _BV(TWCMD0);
                return;
            }

            // Call onI2cAddrReceive callback
            g_i2c.rxBufferIndex = 0;
            if (!onI2cAddrReceive(g_i2c.addr, g_i2c.startCount)) {

                // send NACK
                TWSCRB = _BV(TWHNM)
                       | _BV(TWAA)
                       | _BV(TWCMD1)
                       | _BV(TWCMD0);
                return;
            }

            // Master read operation detected
            if ((i2cStatus & _BV(TWDIR))) {
                g_i2c.txBufferLength = 0;

                // Call onI2cRequest callback
                onI2cRequest();  // load Tx buffer with data
                g_i2c.txBufferIndex = 0;
            }

            // Master write operation detected
            else {
                g_i2c.rxBufferLength = 0;
            }
        }

        // Stop condition detected
        else {

            // Master read operation detected
            if ((i2cStatus & _BV(TWDIR))) {

                // Call onI2cStop callback
                onI2cStop();
            }

            // Master write operation detected
            else {

                // Call onI2cReceive callback
                g_i2c.rxBufferIndex = 0;
                onI2cReceive(g_i2c.rxBufferLength);
            }
            g_i2c.startCount = -1;

            TWSSRA = _BV(TWASIF);  // clear interrupt
            return;
        }
    }

    // A data byte has been successfully received
    else if ((i2cStatus & _BV(TWDIF))) {

        // Master read operation detected
        if ((i2cStatus & _BV(TWDIR))) {
            if (g_i2c.txBufferIndex < g_i2c.txBufferLength) {
                TWSD = g_i2c.buffer[g_i2c.txBufferIndex++];
            }

            // Buffer overrun
            else {

                // Wait for any START condition
                TWSCRB = _BV(TWHNM)
                       | _BV(TWCMD1);
                return;
            }
        }

        // Master write operation detected (a data byte has been received)
        else {
            if (g_i2c.rxBufferLength < I2C_BUFFER_LENGTH) {
                g_i2c.buffer[g_i2c.rxBufferLength++] = TWSD;
            }

            // Buffer overrun
            else {

                // send NACK and wait for any START condition
                TWSCRB = _BV(TWHNM)
                       | _BV(TWAA)
                       | _BV(TWCMD1);
                return;
            }
        }
    }

    // Send ACK
    TWSCRB  = _BV(TWHNM)
            | _BV(TWCMD1)
            | _BV(TWCMD0);
}


// Core
int main(void)
{
    // Init hardware
    initIO();
    initAdc();
    initI2C();

    // Global inits
    g_config = CONFIG_INVERT_INPUT_0
             | CONFIG_INVERT_INPUT_1
             | CONFIG_INVERT_INPUT_2
             | CONFIG_INVERT_INPUT_3;  // all inputs inverted

    // Enable interrupts
    sei();

    // Main loop
    while (1) {

        // Read inputs
        g_inputs = ((GET_INPUT_0 ^ g_config) & CONFIG_INVERT_INPUT_0)
                 | ((GET_INPUT_1 ^ g_config) & CONFIG_INVERT_INPUT_1)
                 | ((GET_INPUT_2 ^ g_config) & CONFIG_INVERT_INPUT_2)
                 | ((GET_INPUT_3 ^ g_config) & CONFIG_INVERT_INPUT_3);

        // Read ADC
        for (uint8_t adcIndex=0; adcIndex<4; adcIndex++) {

            // Select ADC channel
            ADMUXA = adcIndex;

            // Start conversion
            ADCSRA |= _BV(ADSC);

            // Wait for end of conversion
            while (ADCSRA & _BV(ADSC)) {
            }

            // Value is left adjusted, we read only high 8 bits
            uint8_t adcValue = ADCH;

            if (g_config & (CONFIG_INVERT_ADC_0 << adcIndex)) {
                adcValue ^= 0xff;
            }

            g_adc[adcIndex] = adcValue;
        }
    }

    return 1;
}
